import { ApolloClient, ApolloLink, InMemoryCache } from "@apollo/client";
// import { RestLink } from "apollo-link-rest";

const apolloClient = new ApolloClient({
    // uri: 'https://countries.trevorblades.com/',
    uri: 'http://localhost:4000',
    cache: new InMemoryCache() //acts as a state management
  });

  export default apolloClient;