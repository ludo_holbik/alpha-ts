import React, { useState } from "react";
import { AiFillCaretDown } from "react-icons/ai";
import { BsTrash } from "react-icons/bs";
import styled from "styled-components";
import { RootObject, Record2 } from "../../../@types";
import Button from "../../Fragments/Button/Button";
import data from "../../../data/db.json";
import { get, set } from "lodash";

const Table = styled.table`
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;

`;

const Thead = styled.thead`
  background-color: #282c34;
  color: #caf0f8;
`;

const Tbody = styled.tbody`
  background-color: #dddddd;
  transition: clip 600ms ease-out;
  tbody {
    background-color: #f6de4b;
    tbody {
      background-color: #fe8181;

    }
  }
`;

const ButtonTD = styled.td`
  padding: 0;
  box-shadow: 0 0 2px rgba(33, 33, 33, 0.2);
  &:hover {
    box-shadow: 0 0 11px rgba(207, 165, 165, 0.2);
  }
`;

interface MapProps {
  item: any;
  isPatient?: boolean;
  isRelative?: boolean;
  isPhone?: boolean;
}

const getHeads = (head: any[]) => {
  const extraHead = ["", ...head, ""];

  return (
    <Thead>
      {extraHead.map((head) => (
        <th>{head}</th>
      ))}
    </Thead>
  );
};

const patientHeads = [
  "Identification number",
  "Name",
  "Gender",
  "Risk",
  "Hair length",
  "IQ",
  "Admission date",
  "Last breakdown",
  "Yearly fee",
  "Knows the Joker?",
];

const relativeHeads = [
  "Relative ID",
  "Patient ID",
  "Is alive?",
  "Frequency of visits",
];

const phoneHeads = [
  "Phone ID", 
  "ID of the relative", 
  "Phone"
];

const myMappingFunction = ({
  item,
  isPatient,
  isPhone,
  isRelative,
}: MapProps) => {
  if (isPatient) {
    return patientHeads.map((head) => <td>{item.data[head]}</td>);
  }
  if (isRelative) {
    return relativeHeads.map((head) => <td>{item.data[head]}</td>);
  }
  if (isPhone) {
    return phoneHeads.map((head) => <td>{item.data[head]}</td>);
  }
};

interface RecordProps {
  item: any;
  isToggled: boolean;
  onToggle: any;
  onDelete: any;
  children?: any;
  isPhone?: boolean;
  isRelative?: boolean;
  isPatient?: boolean;
}

const TableRecord = ({
  item,
  onDelete,
  onToggle,
  children,
  isToggled,
  isPhone,
  isRelative,
  isPatient,
}: RecordProps) => {
  const hasKids = Object.keys(item.kids).length > 0;
  const tableData = myMappingFunction({
    item,
    isRelative,
    isPhone,
    isPatient,
  });

  

  return (
    <>
      <Tbody>
        <tr>
          <ButtonTD
            onClick={() => onToggle()}
          >
            <Button hasKidsData={hasKids} disabled={hasKids} toggle={hasKids ? isToggled : false}>
              <AiFillCaretDown
                size="24px"
                color="black"
                {...(!hasKids && { color: "grey" })}
              />
            </Button>
          </ButtonTD>
          {tableData}
          <ButtonTD>
            <Button disabled={hasKids}>
              <BsTrash
                onClick={() => onDelete()}
                color="red"
                size="24px"
              />
            </Button>
          </ButtonTD>
        </tr>
        {(isToggled && hasKids) && <tr><td colSpan={patientHeads.length + 2}><table>{children}</table></td></tr>}
      </Tbody>
    </>
  );
};

const RelativesTable = () => {
  const [patientData, setPatientData] = useState<{
    data: RootObject[];
    trigger: number;
  }>({
    data: data as any,
    trigger: Math.random(),
  });

  const relativesData = (index: number) =>
    patientData.data[index]?.kids.has_relatives?.records ?? [];

  const phonesData = (patientIndex: number) => (relativeIndex: number) =>
    relativesData(patientIndex)[relativeIndex]?.kids?.has_phone?.records ?? [];

  const [toggleList, setToggleList] = useState<string[]>([]);

  const handleToggle = (id: string) => {
    const isAlreadyToggled = toggleList.includes(id);
    if (isAlreadyToggled) {
      setToggleList(toggleList.filter((a: any) => a !== id));
    } else {
      setToggleList([...toggleList, id]);
    }
  };

  const handleRemove = (
    patientID: string,
    relativeID?: string,
    phoneID?: string
  ) => {
    if (patientID && relativeID && phoneID) {
      const patientIndex = patientData.data.findIndex(
        (a) => a.data["Identification number"] === patientID
      );
      const relativeIndex = patientData.data[
        patientIndex
      ].kids.has_relatives.records.findIndex(
        (b) => b.data["Relative ID"] === relativeID
      );

      const pathToMyPhones = `data.[${patientIndex}].kids.has_relatives.records[${relativeIndex}].kids.has_phone.records`;
      const pathToMyRelative = `data.[${patientIndex}].kids.has_relatives.records[${relativeIndex}]`;
      const myPhones: Record2[] = get(patientData, pathToMyPhones);
      const myRelative = get(patientData, pathToMyRelative);

      // console.log(myPhones, myRelative, pathToMyPhones);
      const myPhonesWithRemovedOne = myPhones.filter(
        (c) => c.data["Phone ID"] !== phoneID
      );

      const newState = set(patientData, pathToMyPhones, myPhonesWithRemovedOne);

      // console.log("NEW STATE setting", newState);
      setPatientData({ ...newState, trigger: Math.random() });
    } else if (patientID && relativeID) {
      const myPatients = patientData.data.reduce<RootObject[]>((acc, curr) => {
        if (curr.data["Identification number"] === patientID) {
          const newRelatives = curr?.kids.has_relatives.records.filter(
            (b) => b.data["Relative ID"] !== relativeID
          );

          return [
            ...acc,
            { ...curr, kids: { has_relatives: { records: newRelatives } } },
          ];
        } else {
          return [...acc, curr];
        }
      }, []);

      setPatientData({ data: myPatients, trigger: Math.random() });
    } else if (patientID) {
      const myPatient = patientData.data.filter(
        (a) => a.data["Identification number"] !== patientID
      );
      setPatientData({ data: myPatient, trigger: Math.random() });
    }
  };

  return (
    <Table>
      {getHeads(patientHeads)}
      {patientData.data.map((patient, index) => {
        const patientID = patient.data["Identification number"];

        return (
          <TableRecord
            item={patient}
            isPatient={true}
            isToggled={toggleList.includes(patientID)}
            onToggle={() => handleToggle(patientID)}
            onDelete={() => handleRemove(patientID)}
          >
            {getHeads(relativeHeads)}
            {relativesData(index).map((relative, relativeIndex) => {
              const relativeID = relative.data["Relative ID"];

              return (
                <TableRecord
                  item={relative}
                  isRelative={true}
                  isToggled={toggleList.includes(relativeID)}
                  onToggle={() => handleToggle(relativeID)}
                  onDelete={() => handleRemove(patientID, relativeID)}
                >
                  {getHeads(phoneHeads)}
                  {phonesData(index)(relativeIndex).map((phone) => {
                    const phoneID = phone.data["Phone ID"];

                    return (
                      <TableRecord
                        item={phone}
                        isPhone={true}
                        onToggle={() => handleToggle(phoneID)}
                        onDelete={() =>
                          handleRemove(patientID, relativeID, phoneID)
                        }
                        isToggled={toggleList.includes(phoneID)}
                      />
                    );
                  })}
                </TableRecord>
              );
            })}
          </TableRecord>
        );
      })}
    </Table>
  );
};

export default RelativesTable;
