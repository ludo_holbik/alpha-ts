import React from "react";
import styled from "styled-components";
import RelativesTable from "../RelativesTable/RelativesTable";
import "./style.css";

const TableWrapper = styled.div`
  width: 90vw;
  margin: 2em auto;
`;

const PatientsTable = () => {

  return (
    <TableWrapper>
      <RelativesTable />
    </TableWrapper>
  );
};

export default PatientsTable;
