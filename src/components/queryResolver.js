import React from "react";
import { useQuery, gql } from "@apollo/client";
import CountriesList from './CountriesList/CountriesList'

const COUNTRIES_QUERY = gql`
{
    data {
        data
    }
}
        
`;

function QueryResolver() {
    const { data, loading, error } = useQuery(COUNTRIES_QUERY);

    if (loading) return <div>"Loading...";</div>
    if (error) return <pre>{error.message}</pre>

    // console.log(data, 'bla')
    return (
        <div>
            {/* <CountriesList countries={data.countries} /> */}\
            
        </div>
    );

}

export default QueryResolver
