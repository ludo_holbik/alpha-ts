import React from "react";
import styled from "styled-components";

const ButtonStyled = styled.button<any>`
  width: 100%;
  cursor: ${(props) => (props.$hasKidsData ? `pointer` : "")};
  height: 40px;
  overflow: hidden;
  transition: all 0.3s ease-out;
  transform: ${(props) => (props.$toggle ? `rotateX(180deg)` : "")};
  background: transparent;
  border: none;
`;

interface Props {
  children?: React.ReactNode;
  onClick?: () => void;
  toggle?: boolean;
  hasKidsData?: boolean;
  disabled?: boolean;
}

const Button: React.FC<Props> = ({
  children,
  onClick,
  toggle,
  hasKidsData,
  disabled,
}) => {
  return (
    <ButtonStyled
      onClick={onClick}
      $toggle={toggle}
      $hasKidsData={hasKidsData}
      disabled
    >
      {children}
    </ButtonStyled>
  );
};

export default Button;
