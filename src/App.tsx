import React from 'react';
import './App.css';
import apolloClient from "./ApolloClient/client";
import { ApolloProvider } from '@apollo/client';
import QueryResolver from './components/queryResolver'
import PatientsTable from './components/Tables/PatientsTable/PatientsTable'
// import data from './data/db.json'
// import ExchangeRatesPage from './ExchangeRatesPage';


// const parsedData = JSON.parse(data)
// console.log(parsedData,'aa')

function App() {
  return (
    <ApolloProvider client={apolloClient}>
      <header className="App-header">
          <h1>Patients table</h1>
      </header>
      <body>
        {/* <div className="queryResolver">
          <QueryResolver></QueryResolver>
        </div> */}
        <PatientsTable />
      </body>
    </ApolloProvider>
  );
}

export default App;
